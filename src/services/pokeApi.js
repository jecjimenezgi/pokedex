/**
 * @param {string} name Name asociated to pokemon.
 * @returns {Promise} Object
 */
const getUrl = (url) => {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
  };
  return fetch(url, requestOptions);
};

/**
 * @param {(number|string)} offset Interval's initial value.
 * @param {(number|string)} limit Interval's final value.
 * @returns {Promise} Object with count, next, previous and results properties and results
 * array wih all the pokemon with the given offset and limit and propierties name, url.
 */
const getPokemons = (offset, limit) => {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
  };
  return fetch(
    `https://pokeapi.co/api/v2/pokemon/?offset=${offset}&limit=${limit}`,
    requestOptions
  );
};

/**
 * @param {(number|string)} id Number asociated to pokemon.
 * @returns {Promise} Object some propierties from pokemon with the given id.
 */
const getPokemonById = (id) => {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
  };
  return fetch(`https://pokeapi.co/api/v2/pokemon/${id}/`, requestOptions);
};

/**
 * @param {string} name Name asociated to pokemon.
 * @returns {Promise} Object some propierties from pokemon with the given name.
 */
const getPokemonByName = (name) => {
  return getPokemons(0, 1500)
    .then((response) => response.json())
    .then((obj) => {
      var pokemons = obj.results;
      var pokemon = pokemons.filter((p) => p.name === name)[0];
      var url = pokemon.url;
      return getUrl(url);
    })
    .catch((error) => {
      console.error(error);
    });
};

/**
 * @returns {Promise} Object with count, next, previous and results properties and results
 * array wih all the pokemon and propierties name, url.
 */
const getTypes = () => {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
  };
  return fetch("https://pokeapi.co/api/v2/type/", requestOptions);
};

/**
 * @param {(number|string)} typeId Number asociated to type.
 * @returns {Promise} Object with
 * damage_relations, game_indices, generation, id, move_damage_class, moves, name, names, pokemon properties
 * and for pokemon several objects with pokemon propertie and this with name, url.
 */
const getTypeById = (typeId) => {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
  };
  return fetch(`https://pokeapi.co/api/v2/type/${typeId}/`, requestOptions);
};

const getTypeByName = (name) => {
  return getTypes()
    .then((response) => response.json())
    .then((obj) => {
      var types = obj.results;
      var type = types.filter((t) => t.name === name)[0];
      var url = type.url;
      return getUrl(url);
    })
    .catch((error) => {
      console.error(error);
    });
};

/**
 * @param {(number|string)} offset Interval's initial value.
 * @param {(number|string)} limit Interval's final value.
 * @returns {Promise} Object with name, img, stats and types properties.
 */
const getCardsByRange = (offset, limit) => {
  return getPokemons(offset, limit)
    .then((response) => response.json())
    .then((obj) => {
      var pokemons = obj.results;
      var pokemonsArray = pokemons.map((p) => {
        return {
          name: p.name,
          url: p.url,
        };
      });
      var cardsPromises = pokemonsArray.map((p) => {
        return getUrl(p.url)
          .then((response) => response.json())
          .then((obj) => {
            return {
              name: obj.name,
              img: obj.sprites.front_default,
              stats: {
                hp: obj.stats[0].base_stat,
                attack: obj.stats[1].base_stat,
                defense: obj.stats[2].base_stat,
                specialAttack: obj.stats[3].base_stat,
                specialDefense: obj.stats[4].base_stat,
                speed: obj.stats[5].base_stat,
              },
              type: obj.types[0].type.name,
            };
          });
      });
      return Promise.all(cardsPromises);
    })
    .catch((error) => {
      console.error(error);
    });
};

/**
 * @param {string} name Pokemon's id.
 * @returns {Promise} Object with name, img, stats and types properties.
 */
const getCardById = (id) => {
  return getPokemonById(id)
    .then((response) => response.json())
    .then((obj) => {
      return {
        name: obj.name,
        img: obj.sprites.front_default,
        stats: {
          hp: obj.stats[0].base_stat,
          attack: obj.stats[1].base_stat,
          defense: obj.stats[2].base_stat,
          specialAttack: obj.stats[3].base_stat,
          specialDefense: obj.stats[4].base_stat,
          speed: obj.stats[5].base_stat,
        },
        type: obj.types[0].type.name,
      };
    })
    .catch((error) => console.error(error));
};

/**
 * @param {string} name Pokemon's name.
 * @returns {Promise} Object with name, img, stats and types properties.
 */
const getCardByName = (name) => {
  return getPokemonByName(name)
    .then((response) => response.json())
    .then((obj) => {
      return {
        name: obj.name,
        img: obj.sprites.front_default,
        stats: {
          hp: obj.stats[0].base_stat,
          attack: obj.stats[1].base_stat,
          defense: obj.stats[2].base_stat,
          specialAttack: obj.stats[3].base_stat,
          specialDefense: obj.stats[4].base_stat,
          speed: obj.stats[5].base_stat,
        },
        type: obj.types[0].type.name,
      };
    })
    .catch((error) => console.error(error));
};

/**
 * @param {string} type Pokemon's type.
 * @param {(number|string)} offset Interval's initial value.
 * @param {(number|string)} limit Interval's final value.
 * @returns {Promise} Object with name, img, stats and types properties.
 */
const getCardsByType = (type, offset, limit) => {
  return getTypes()
    .then((response) => response.json())
    .then((obj) => {
      var types = obj.results;
      var _type = types.filter((t) => t.name === type)[0];
      var url = _type.url;
      return getUrl(url)
        .then((response) => response.json())
        .then((obj) => {
          var pokemonUrls = obj.pokemon
            .map((p) => p.pokemon.url)
            .splice(offset, limit);
          var cardsPromises = pokemonUrls.map((p) => {
            return getUrl(p)
              .then((response) => response.json())
              .then((obj) => {
                return {
                  name: obj.name,
                  img: obj.sprites.front_default,
                  stats: {
                    hp: obj.stats[0].base_stat,
                    attack: obj.stats[1].base_stat,
                    defense: obj.stats[2].base_stat,
                    specialAttack: obj.stats[3].base_stat,
                    specialDefense: obj.stats[4].base_stat,
                    speed: obj.stats[5].base_stat,
                  },
                  type: obj.types[0].type.name,
                };
              });
          });
          return Promise.all(cardsPromises);
        });
    })
    .catch((error) => {
      console.error(error);
    });
};

/**
 * @param {string} type Pokemon's type.
 * @returns {Promise} number of elements for specified type.
 */
const countType = (type) => {
  return getTypes()
    .then((response) => response.json())
    .then((obj) => {
      var types = obj.results;
      var _type = types.filter((t) => t.name === type)[0];
      var url = _type.url;
      return getUrl(url)
        .then((response) => response.json())
        .then((obj) => {
          return obj.pokemon.length;
        });
    })
    .catch((error) => console.error(error));
};

/**
 * @returns {Promise} number of pokemons.
 */
const countPokemons = () => {
  return getPokemons()
    .then((response) => response.json())
    .then((obj) => {
      return obj.count;
    });
};

export const pokeApiOracle = {
  lowLevel: {
    getPokemonByName,
    getPokemonById,
    getTypes,
    getTypeById,
    getTypeByName,
  },
  hightLevel: {
    getCardsByRange,
    getCardById,
    getCardByName,
    getCardsByType,
    countType,
    countPokemons,
  },
};
