import { createContext, useContext, useState } from "react";

const infoContext = createContext();


const useProvideInfo = () => {

    const [username, setUsername] = useState(null);
    const [pagination, setPagination] = useState(20);
    const [theme, setTheme] = useState("ligth");

    const saveUsename = (username) => {
        setUsername(username);
    }

    const saveTheme = (theme) => {
        setTheme(theme);
    }

    const savePagination = (pagination) => {
        setPagination(pagination);
    }

    return {
        username,
        theme,
        pagination,
        saveUsename,
        saveTheme,
        savePagination
    }

}


export const ProvideInfo = ({ children }) => {
    const info = useProvideInfo();
  
    return <infoContext.Provider value={info}>{children}</infoContext.Provider>;
  };

export const useInfo = () => useContext(infoContext);








