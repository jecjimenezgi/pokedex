import "./App.css";
import Login from "./components/Login/Login.js";
import Home from "./components/Home/Home.js";
import {
  HashRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import ProtectedRoute from "./components/Routes/ProtectedRoute.js";
import { ProvideAuth } from "./Provider/AuthProvider.js";
import { ProvideInfo } from "./Provider/InfoProvider.js";

function App() {
  return (
    <ProvideAuth>
      <ProvideInfo>
      <Router>
        <div className="App">
          <Switch>
            <Route path="/login">
              <Login />
            </Route>
            <ProtectedRoute path="/home">
              <Home />
            </ProtectedRoute>
            <Route path="/">
              <Redirect to="/home" />
            </Route>
          </Switch>
        </div>
      </Router>
      </ProvideInfo>
    </ProvideAuth>
  );
}

export default App;
