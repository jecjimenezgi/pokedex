import "../Grid/Grid.css";
import "./Login.css";
import { useHistory } from "react-router-dom";
import { useAuth } from "../../Provider/AuthProvider.js" 
import {useInfo} from "../../Provider/InfoProvider.js";

const Login = () => {

  const history = useHistory();
  const { signIn } = useAuth();
  const { saveUsename } = useInfo();
  const login = (event) => {
    event.preventDefault();
    let usernameInputElement = event.target
      .closest("form")
      .querySelector("input[name='username']");
    if (usernameInputElement.value){
      saveUsename(usernameInputElement.value);
      signIn(() => {
        history.push("/home");
      })
    }else{
    }

  };
  return (
    <div className="Login">
      <form>
        <div className="row">
          <div className="col-12">
            <div id="inputs">
              <label>
                <h2>Username</h2>
                <input type="text" id="username" name="username" required />
              </label>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div>
              <button type="submit" onClick={login}>
                <h2>Login</h2>
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Login;
