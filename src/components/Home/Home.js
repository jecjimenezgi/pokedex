import Navbar from "./components/Navbar/Navbar.js";
import Dashboard from "./components/Dashboard/Dashboard.js";

const Home = () => {

  return (
    <div className="Home">
      <div>
      <Navbar />
      </div>
      <div>
      <Dashboard />
      </div>
    </div>
  );
};

export default Home;
