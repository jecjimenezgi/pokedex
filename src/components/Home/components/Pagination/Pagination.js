import "./Pagination.css";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import {useParams } from "react-router-dom";

const Pagination = ({count, url, type = null }) => {

  const params = useParams()
  const [viewPagination, setViewPagination] = useState(null);



  useEffect(() => {
    let nurl = url;
    if (type){
     nurl = `${url}/${type}`
    }

      let links = [];
      for (
        let i = 0;
        i < Math.ceil((Number(count)) / Number(params.limit));
        i++
      ) {
        let k =  Number(params.limit) * i;
        let end = k + Number(params.limit)
        if (end > Number(count)) {
          end = Number(count);
        }
        links.push(
          <div key={i} className="col-3">
            <Link
              to={`${nurl}/${k}/${params.limit}`}
              style={{
                textDecoration: "none",
                color: "black",
              }}
            >
              <div id="pagination-link">
              <h3>
                {k}-{end}
              </h3>
              </div>
            </Link>
          </div>
        );
      
      setViewPagination(links);
    }
  }, [params.offset, params.limit, count, url,type]);

  return (
    <div className="Pagination">
      <div className="row">{viewPagination}</div>
    </div>
  );
};

export default Pagination;
