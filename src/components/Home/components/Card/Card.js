import "./Card.css"

const Card = ({name,img,stats,type}) => {
    return (
        <div className="Card">
            <div className="row">
                <div className="col-12">
                    <h2>{name}</h2>
                </div>
            </div> 
            <div className="row">
                <div className="col-12">
                    <img src={img} alt={name} className="Card-img" />
                </div>
            </div> 
            <div className="row">
                <div className="col-4">
                    <h5>Hp:{stats.hp}</h5>
                </div>
                <div className="col-4">
                    <h5>Attack:{stats.attack}</h5>
                </div>
                <div className="col-4">
                    <h5>Defense:{stats.defense}</h5>
                </div>
                <div className="col-4">
                    <h5>SpecialAttack:{stats.specialAttack}</h5>
                </div>
                <div className="col-4">
                    <h5>SpecialDefense:{stats.specialDefense}</h5>
                </div>
                <div className="col-4">
                    <h5>Speed:{stats.speed}</h5>
                </div>
            </div> 
            <div className="row">
                <div className="col-12">
                    <h5>Type:{type}</h5>
                </div>
            </div> 
        </div>
    )
}

export default Card