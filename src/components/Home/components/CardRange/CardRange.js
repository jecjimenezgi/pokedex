import Card from "../Card/Card.js";
import { pokeApiOracle } from "../../../../services/pokeApi.js";
import { useEffect, useState } from "react";
import {useParams } from "react-router-dom";

const CardRange = () => {
  const params = useParams();
  const [cards, setCards] = useState();
  const [viewCards, setViewCards] = useState(null);
  useEffect(() => {
    pokeApiOracle.hightLevel.getCardsByRange(params.offset, params.limit).then((cards) => {
      setCards(cards);
    });
  }, [params.offset, params.limit]);

  useEffect(() => {
    if (cards) {
      setViewCards(
        cards.map((card) => {
          return (
            <Card
              key={card.name}
              name={card.name}
              img={card.img}
              stats={card.stats}
              type={card.type}
            />
          );
        })
      );
    }
  }, [cards]);

  return (
    <div className="row" id="cards">
      {viewCards}
    </div>
  );
};

export default CardRange;
