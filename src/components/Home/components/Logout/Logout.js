import "./Logout.css"
import { useAuth } from "../../../../Provider/AuthProvider.js";

const Logout = () => {

  const {signOut} = useAuth()
  return (
    <div className="Logout">
      <button
        onClick={() => {
            signOut(() => {})
        }}
      >
        <h2>Logout</h2>
      </button>
    </div>
  );
};

export default Logout;
