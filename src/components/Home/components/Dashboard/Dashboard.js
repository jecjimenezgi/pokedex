import "./Dashboard.css";
import CardRange from "../CardRange/CardRange.js";
import CardTypeRange from "../CardTypeRange/CardTypeRange.js";
import CardUnique from "../CardUnique/CardUnique.js";
import Pagination from "../Pagination/Pagination.js";
import {
  useRouteMatch,
  Route,
  Switch,
  Redirect,
  useHistory,
} from "react-router-dom";
import { pokeApiOracle } from "../../../../services/pokeApi.js";
import { useEffect, useState } from "react";
import { useInfo } from "../../../../Provider/InfoProvider";

const Dashboard = () => {
  const { pagination } = useInfo();
  const { path, url } = useRouteMatch();
  const [countPokemons, setCountPokemons] = useState();
  const [countTypes, setCountType] = useState();
  const [type, setType] = useState();
  const [types, setTypes] = useState();
  const [viewSelectTypes, setViewSelectTypes] = useState(null);
  const history = useHistory();

  let eventSelectType = (event) => {
    let type = event.target.value;
    pokeApiOracle.hightLevel
      .countType(type)
      .then((count) => {
        setCountType(count);
        setType(type);
        history.push(`${url}/type/${type}/0/${pagination}`);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const eventSearchPokemon = (event) => {
    event.preventDefault();
    const search = event.target.closest("form").querySelector("input").value;
    history.push(`${url}/unique/${search}`);
  };

  useEffect(() => {
    pokeApiOracle.hightLevel
      .countPokemons()
      .then((countPokemons) => setCountPokemons(countPokemons));
    pokeApiOracle.lowLevel
      .getTypes()
      .then((response) => response.json())
      .then((types) => {
        setTypes(types.results);
      });
  }, []);

  useEffect(() => {
    if (types) {
      let optionTypes = types.map((type) => {
        return (
          <option key={type.name} value={type.name}>
            {type.name}
          </option>
        );
      });
      setViewSelectTypes(optionTypes);
    }
  }, [types]);

  return (
    <div className="Dasboard">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h2>Welcome to pokemon</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-5">
            <div id="selectType">
              <form>
                <label>
                  <h2>Select Type</h2>
                </label>
                <select onChange={eventSelectType}>{viewSelectTypes}</select>
              </form>
            </div>
          </div>
          <div className="col-5" id="selectPokemon">
            <form>
              <label>
                <h2>Pokemon</h2>
              </label>

              <div className="row">
                <div className="col-5">
                  <h3>
                    <input placeholder="name/id"></input>
                  </h3>
                </div>
                <div className="col-5">
                  <button type="submit" onClick={eventSearchPokemon}>
                    Search
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <Switch>
          <Route path={`${path}/cards/:offset/:limit`}>
            <Pagination url={`${url}/cards`} count={countPokemons}></Pagination>
            <CardRange id="cards"></CardRange>
          </Route>
          <Route path={`${path}/unique/:name`}>
            <CardUnique id="unique"></CardUnique>
          </Route>
          <Route path={`${path}/type/:name/:offset/:limit`}>
            <Pagination
              url={`${url}/type`}
              count={countTypes}
              type={type}
            ></Pagination>
            <CardTypeRange id="type"></CardTypeRange>
          </Route>
          <Route path="/home">
            <Redirect to={`${path}/cards/0/${pagination}`} />
          </Route>
        </Switch>
      </div>
    </div>
  );
};

export default Dashboard;
