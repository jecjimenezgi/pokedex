import { useParams } from "react-router-dom";
import { pokeApiOracle } from "../../../../services/pokeApi.js";
import { useEffect, useState } from "react";
import Card from "../Card/Card.js";

const CardUnique = () => {
  const params = useParams();
  const [unique, setUnique] = useState();
  const [viewCard, setViewCard] = useState(null);

  useEffect(() => {
    if (params.name) {
      pokeApiOracle.hightLevel
        .getCardById(params.name.toLowerCase())
        .then((card) => {
          setUnique(card);
        });
    }
  }, [params.name]);

  useEffect(() => {
    if (unique) {
      setViewCard(
        <Card
          name={unique.name}
          img={unique.img}
          stats={unique.stats}
          type={unique.type}
        ></Card>
      );
    }
  }, [unique]);

  return <div id="CardUnique">{viewCard}</div>;
};

export default CardUnique;
