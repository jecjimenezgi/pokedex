import "./Navbar.css";
import picachu from "../../../../assets/picachu.png";
import Logout from "../Logout/Logout.js";
import { useInfo } from "../../../../Provider/InfoProvider.js";
import { Link,useHistory } from "react-router-dom";
const Navbar = () => {
  const history = useHistory();
  const { username,savePagination } = useInfo();

  const setPagination20 = (event) => {
      
    event.target.closest(".Navbar").querySelector("#pag-20").style["background-color"] = "green";
    event.target.closest(".Navbar").querySelector("#pag-50").style["background-color"] = "red";
    event.target.closest(".Navbar").querySelector("#pag-100").style["background-color"] = "red";
    savePagination(20);
    history.push("/home");
  }

  const setPagination50 = (event) => {
    event.target.closest(".Navbar").querySelector("#pag-20").style["background-color"] = "red";
    event.target.closest(".Navbar").querySelector("#pag-50").style["background-color"] = "green";
    event.target.closest(".Navbar").querySelector("#pag-100").style["background-color"] = "red";
    savePagination(50);
    history.push("/home");
  }

  const setPagination100 = (event) => {
    event.target.closest(".Navbar").querySelector("#pag-20").style["background-color"] = "red";
    event.target.closest(".Navbar").querySelector("#pag-50").style["background-color"] = "red";
    event.target.closest(".Navbar").querySelector("#pag-100").style["background-color"] = "green";
    savePagination(100);
    history.push("/home");
  }

  return (
    <div className="Navbar">
      <div className="Navbar-brand">
        <div className="row">
          <div className="col-12">
            <div className="row">
              <div className="col-12">
                <h3>{username}</h3>
              </div>
              <div className="col-12">
                <img src={picachu} alt="picachu"></img>
              </div>
              <div className="col-12">
                <Link
                  to="/home"
                  style={{
                    textDecoration: "none",
                    color: "black",
                  }}
                >
                  <div className="Navbar-button">
                    <h2>Home</h2>
                  </div>
                </Link>
              </div>
              <div className="col-12">
                  <div className="Navbar-button" id="pag-20" onClick={setPagination20}>
                    <h2>Pag 20</h2>
                  </div>
              </div>
              <div className="col-12">
                  <div className="Navbar-button" id="pag-50" onClick={setPagination50}>
                    <h2>Pag 50</h2>
                  </div>
              </div>
              <div className="col-12">
                  <div className="Navbar-button" id="pag-100" onClick={setPagination100}>
                    <h2>Pag 100</h2>
                  </div>
              </div>
 
              <div className="col-12">
                <Logout />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
